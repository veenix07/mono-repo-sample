package com.example.todoapi.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping
	public ResponseEntity<?> hello(){
		// demo commit body
		return ResponseEntity.ok("hello, world - with another changes!!!");
	}

}
