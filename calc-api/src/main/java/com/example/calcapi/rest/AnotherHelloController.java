package com.example.calcapi.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/another")
public class AnotherHelloController {

	@Autowired
	BuildProperties buildProperties;

	@GetMapping
	public ResponseEntity<?> anotherHello(){
		return ResponseEntity.ok("Something did change... another day...");
	}

	@GetMapping("/info")
	public ResponseEntity<?> findInfo(){
		return ResponseEntity.ok(buildProperties);
	}

}
