package com.example.calcapi.sample;

public class SampleImpl implements SampleInterface{
    @Override
    public String sayHello() {
        return "Banana";
    }

    @Override
    public String sayBye() {
        return "Bye";
    }
}
