package com.example.calcapi.sample;

public class AnotherSampleImpl implements SampleInterface {
    @Override
    public String sayHello() {
        return "Another hello";
    }

    @Override
    public String sayBye() {
        return null;
    }
}
