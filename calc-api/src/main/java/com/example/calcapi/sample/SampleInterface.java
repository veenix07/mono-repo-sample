package com.example.calcapi.sample;

public interface SampleInterface {

    String sayHello();

    String sayBye();

}
